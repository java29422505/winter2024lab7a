public class Card{
	private String suit;
	private String value;

	// constractor to set the default value

	public Card(String suit, String value){

		if (suit == null || value == null){

		// throwing exception if the value is null

			throw new IllegalArgumentException ("suit and value can not be null");
		}

		this.suit= suit;
		this.value= value;

	}
	// getting the suit
	public String getSuit(){
		return this.suit;
	}
	// geting the value
	public String getValue(){
		return this.value;
	}
	// toString method will return the value of a card

	public String toString(){


		String printCard = this.value+ " of "+this.suit +"\n";

		return printCard;
		
	}
	// this will calcullate the scores of the cards and returns its value.
	
	public double calculateScore(){

			double valueScore=0;

			double suitScore=0;


			if(this.value.equals("Jack")){

				valueScore=11.0;
			}
			else if(this.value.equals("Queen")){

				valueScore= 12.0;
			}
			else if(this.value.equals("King")){
				valueScore=13.0;

			}
			else if (this.value.equals("Ace")){
				valueScore=14.0;
			}
			else{

				valueScore = Double.parseDouble(this.value);
			}

			if (this.suit.equals("Hearts")){

				suitScore= 0.4;
			}
			if (this.suit.equals("Diamonds")){

				suitScore= 0.2;
			}
			 if (this.suit.equals("Clubs")){

				suitScore= 0.1;
			}

			if(this.suit.equals("Spades")){

				suitScore= 0.3;
			}
			

			return valueScore+suitScore;
		}
}

