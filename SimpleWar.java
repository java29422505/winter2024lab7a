public class SimpleWar{
	
	public static void main(String[] args){

		// making deck object here 

		Deck deck = new Deck();

		// this will shuffle the deck

		deck.shuffleDeck();

		System.out.println(deck);


		// creating the first and second player's points

		double player1_point= 0;
		double player2_point=0;

		// this will loop until the deck of card ends
		while(deck.length()>0){

			// this will draw the first player's hand 
			Card firstCard = deck.drawTopCard();
			// this will draw the second player's hand 
			Card secondCard= deck.drawTopCard();

			// printing the player's point before the the game.

			System.out.println("Player one points before playing is: "+player1_point+" points");

			System.out.println("Player two point before playing is: "+player2_point+" points");

			// this will show the player's scores

			double player1_Score= firstCard.calculateScore();

			double player2_Score= secondCard.calculateScore();

			System.out.println("");

			// this will print the player's hand and its value.

			System.out.println("Player 1's first Card drawn: "+firstCard+ " "+firstCard.calculateScore());
			System.out.println("Player two first Card drawon: "+secondCard+" "+secondCard.calculateScore());

			// if player 1's score is greater than player2 then player one wins

			if(player1_Score > player2_Score){

				System.out.println("Player one wins with this : "+player1_Score+" points");

				// increments the point if the player wins

				player1_point++;

			}
			else{
				System.out.println("Player two wins with this : "+player2_Score+" points");

				player2_point++;


			}

			// printing the player's points after the game.

			System.out.println("Player one points After playing is: "+player1_point+" points");

			System.out.println("Player two point After playing is: "+player2_point+" points");
		}
		if (player1_point > player2_point){

			System.out.println("Congrats the winner of the game is: Player1");
		}
		else {
			System.out.println("Congrats the winner of the game is: Player2");

		}


	}
}