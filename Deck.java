import java.util.Random;

public class Deck{
	
	private Card [] deck;
	private int numberOfCards;
	private Random rng;

	//deck {2H, 2C, 2S, 2D, 3H, 3C, 3S, 3D .... AH, AC, AD, AS}
//numofCard                                                   ^

	public Deck (){

		this.rng= new Random();

		this.deck = new Card[52];

		this.numberOfCards=numberOfCards;

		String[] suit = {"Hearts","Diamonds","Clubs","Spades"};

		String[] value = {"2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"};


		for(int i = 0; i<suit.length; i++){

			for(int j=0;j<value.length;j++){

				deck[this.numberOfCards]= new Card(suit[i], value[j]);

				this.numberOfCards++;
			}


		}

	}
	public int length() {

		return this.numberOfCards;
	}
	public Card drawTopCard(){

		if (this.numberOfCards>0){

			Card topCard = deck[numberOfCards-1];

			this.numberOfCards--;

			return topCard;
		}
		else {

			return null;
		}
		
	}
	public String toString(){

		String deckString = "";

		for(Card card : deck){

			if (card != null){

				deckString += card.toString();

			}

		}

		return deckString.toString();

	}
	public void shuffleDeck(){


		for(int i =0; i<this.deck.length;i++){

			int randomSwap= rng.nextInt(this.deck.length);
			
			Card temp= deck[randomSwap];

			deck[randomSwap]= deck[i];

			deck[i]= temp;

		}
	}
	
}